const helpers = require('../app/helper');
const assert = require('assert');

describe('Helper Functions', function() {
    describe('getSingleComicPromise(1)', function() {
        let result;
        before(async function() {
            result = await helpers.getSingleComicPromise(1);
        })
        it('should return an object with index 1', async function() {
            assert.equal(result.index, 1);
        });
        it('should return an object with first image', async function() {
            assert.equal(result.image, '//imgs.xkcd.com/comics/barrel_cropped_(1).jpg');
        });
    });

    describe('getComicsFromWebsite(1)', function() {
        let result;
        before(async function() {
            result = await helpers.getComicsFromWebsite(1);
        })
        it('should return an array with 10 items', async function() { 
            assert.equal(result.length, 10);
        });

        it('should return array with objects index 11-20', async function() {
            assert.equal(result[0].index, 11);
            assert.equal(result[9].index, 20);
        });

        it('should return array with objects images 11-20', async function() {
            assert.equal(result[0].image, '//imgs.xkcd.com/comics/barrel_mommies.jpg');
            assert.equal(result[9].image, '//imgs.xkcd.com/comics/ferret.jpg');
        });
    });
});