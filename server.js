var express = require('express');
var app = express();
var http = require('http').Server(app);
var bodyParser = require('body-parser');
var helpers = require('./app/helper');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(express.static(__dirname + '/client/build'));
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.post('/getComics', async function (req, res) {
    let page = req.body.page;
    let comics = await helpers.getComicsFromWebsite(page);
    res.send({ comics });
});

//_____Start Server on Port 3001_____/
http.listen(3001, function () {
    console.log('listening on *:3001');
});