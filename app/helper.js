const request = require('request');
const cheerio = require('cheerio');

module.exports = {
    getComicsFromWebsite: async function(page) {
        let startComicIndex = page * 10 + 1;
        let getComicPromises = [];
        let result = [];
        
        // add promises for single comic to array
        for (let i = startComicIndex; i < startComicIndex + 10; i++) {
            getComicPromises.push(this.getSingleComicPromise(i));
        }

        // execute all promises and return result
        await Promise.all(getComicPromises)
            .then(values => {
                result = values;
            });

        return result;
    },
    // returns a promise to fetch a comic
    getSingleComicPromise: function(index) {
        return new Promise(function(resolve, reject) { 
            let url = `https://xkcd.com/${index}`;
            request(url, function (error, response, body) {
                let $ = cheerio.load(body, {decodeEntities: false});

                let image = $('#comic img').attr('src');
                resolve({
                    index,
                    image
                });
            });
        });
    }
}