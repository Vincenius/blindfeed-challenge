import React, { Component } from 'react';
import './App.css';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isInitialized: false,
      loading: true,
      comics: [],
      page: 0
    };
  }

  componentDidMount() {
    window.addEventListener('scroll', this.onScroll, false);
    // get initial images and put them into state
    this.fetchComics();
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.onScroll, false);
  }

  onScroll = () => {
    if ((window.innerHeight + window.scrollY) >= (document.body.offsetHeight - 500) && !this.state.loading) {
      this.fetchComics();
    }
  }

  // get comics based on page parameter
  fetchComics = () => {
    this.setState({
      loading: true,
      page: this.state.page + 1
    });

    fetch("http://localhost:3001/getComics" , {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          page: this.state.page
        })
      })
      .then(res => res.json())
      .then(
        (result) => {
          this.setState({
            isInitialized: true,
            loading: false,
            comics: [...this.state.comics, ...result.comics]
          });
        },
        (error) => {
          this.setState({
            isInitialized: true,
            loading: false,
            error
          });
        }
      )
  }

  render() {
    const { error, isInitialized, comics } = this.state;
    if (error) {
      return <div>Error: {error.message}</div>;
    } else if (!isInitialized) {
      return (<div className="spinner">
        <div className="double-bounce1"></div>
        <div className="double-bounce2"></div>
      </div>);
    } else {
      return (
        <div className="content">
          <header>Number of comics loaded: {comics.length}</header>
          <main className="comics">
              {comics.map(comic => (
                <div key={comic.index} className="image-container">
                  <img src={comic.image} alt={comic.index}/>
                </div>
              ))}
          </main>
          <div className="spinner">
            <div className="double-bounce1"></div>
            <div className="double-bounce2"></div>
          </div>
        </div>
      );
    }
  }
}

export default App;
